namespace :db do
  desc "Fill database with sample data"
  task populate: :environment do
    administrator=User.create!(name: "Giacomo",
                 email: "dgiac85@gmail.com",
                 password: "foobar",
                 password_confirmation: "foobar")
    administrator.toggle!(:admin)
   
  end
end