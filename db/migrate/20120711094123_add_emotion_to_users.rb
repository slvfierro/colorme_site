class AddEmotionToUsers < ActiveRecord::Migration
  def change
    add_column :users, :emotion, :integer
  end
end
