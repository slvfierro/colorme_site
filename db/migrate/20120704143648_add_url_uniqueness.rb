class AddUrlUniqueness < ActiveRecord::Migration
  def self.up
    add_index :videos, :url, :unique=>true
  end

  def self.down
    remove_index :videos, :url
  end
end