class RemoveColumnToComments < ActiveRecord::Migration
  def up
    remove_column :comments, :text
  end

  def down
  end
end
