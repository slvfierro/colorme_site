class RemoveIndexToWatches < ActiveRecord::Migration
  def up
    remove_index :watches, [:user_id, :video_id] 
  end

  def down
  end
end
