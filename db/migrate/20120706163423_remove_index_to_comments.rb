class RemoveIndexToComments < ActiveRecord::Migration
  def up
    remove_index :comments, [:user_id, :video_id] 
  end

  def down
  end
end
