class RemoveEmotionToUsers < ActiveRecord::Migration
  def up
    remove_column :users, :emotion
  end

  def down
  end
end
