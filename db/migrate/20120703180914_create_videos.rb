class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.string :title
      t.datetime :datecreation
      t.string :nameusercreator
      t.integer :valuevideo
      t.integer :user_id

      t.timestamps
    end
    add_index :videos, [:user_id, :created_at] #multiple key index
  end
end
