class CreateFolders < ActiveRecord::Migration
  def change
    create_table :folders do |t|
      t.string :namefolder
      t.string :nameusercreator
      t.integer :user_id

      t.timestamps
    end #inserire un se si vuole modificare l'ordine
    add_index :folders, [:user_id, :created_at] #multiple key index
  end
end
