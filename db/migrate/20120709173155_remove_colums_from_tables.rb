class RemoveColumsFromTables < ActiveRecord::Migration
  def up
    remove_column :comments, :nameuser
    remove_column :folders, :nameusercreator
    remove_column :watches, :emotion
  end

  def down
  end
end
