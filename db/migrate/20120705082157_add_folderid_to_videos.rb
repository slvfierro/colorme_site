class AddFolderidToVideos < ActiveRecord::Migration
  def change
    add_column :videos, :folder_id, :integer
    add_index :videos, :folder_id
  end
end
