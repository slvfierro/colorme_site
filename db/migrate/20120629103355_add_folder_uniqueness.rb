class AddFolderUniqueness < ActiveRecord::Migration
  def up
    add_index :folders, :namefolder, :unique=>true
  end

  def down
    remove_index :folders, :namefolder
  end
end
