class AddEmotionToWatches < ActiveRecord::Migration
  def change
    add_column :watches, :emotion, :integer
  end
end
