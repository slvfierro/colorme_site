class CreateOwns < ActiveRecord::Migration
 def change
    create_table :owns do |t|
      t.column :user_id, :integer
      t.column :folder_id, :integer
      t.column :scorefolder, :integer
      t.column :state, :integer
      t.timestamps
    end
    add_index :owns, :user_id
    add_index :owns, :folder_id
    add_index :owns, [:user_id, :folder_id], unique: true
  end
end