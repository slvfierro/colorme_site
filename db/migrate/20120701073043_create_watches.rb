class CreateWatches < ActiveRecord::Migration
  def change
    create_table :watches do |t|
      t.integer :user_id
      t.integer :video_id
      t.integer :votecolor

      t.timestamps
    end
    add_index :watches, :user_id
    add_index :watches, :video_id
    add_index :watches, [:user_id, :video_id], unique: true
  end
end
