class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.integer :user_id
      t.integer :video_id
      t.integer :text

      t.timestamps
    end
    add_index :comments, :user_id
    add_index :comments, :video_id
    add_index :comments, [:user_id, :video_id], unique: true
  end
end

