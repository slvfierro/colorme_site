# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120713130708) do

  create_table "comments", :force => true do |t|
    t.integer  "user_id"
    t.integer  "video_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "text"
  end

  add_index "comments", ["user_id"], :name => "index_comments_on_user_id"
  add_index "comments", ["video_id"], :name => "index_comments_on_video_id"

  create_table "folders", :force => true do |t|
    t.string   "namefolder"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "folders", ["namefolder"], :name => "index_folders_on_namefolder", :unique => true
  add_index "folders", ["user_id", "created_at"], :name => "index_folders_on_user_id_and_created_at"

  create_table "owns", :force => true do |t|
    t.integer  "user_id"
    t.integer  "folder_id"
    t.integer  "scorefolder"
    t.integer  "state"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "owns", ["folder_id"], :name => "index_owns_on_folder_id"
  add_index "owns", ["user_id", "folder_id"], :name => "index_owns_on_user_id_and_folder_id", :unique => true
  add_index "owns", ["user_id"], :name => "index_owns_on_user_id"

  create_table "users", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.string   "password_digest"
    t.string   "remember_token"
    t.boolean  "admin",           :default => false
    t.integer  "emotion"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["name"], :name => "index_users_on_name", :unique => true
  add_index "users", ["remember_token"], :name => "index_users_on_remember_token"

  create_table "videos", :force => true do |t|
    t.string   "title"
    t.integer  "valuevideo"
    t.integer  "user_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "url"
    t.integer  "folder_id"
    t.string   "description"
  end

  add_index "videos", ["folder_id"], :name => "index_videos_on_folder_id"
  add_index "videos", ["url"], :name => "index_videos_on_url", :unique => true
  add_index "videos", ["user_id", "created_at"], :name => "index_videos_on_user_id_and_created_at"

  create_table "watches", :force => true do |t|
    t.integer  "user_id"
    t.integer  "video_id"
    t.integer  "votecolor"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "watches", ["user_id"], :name => "index_watches_on_user_id"
  add_index "watches", ["video_id"], :name => "index_watches_on_video_id"

end
