// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require_tree .

/* ALERT ALL' ELIMINAZIONE DI UNA CARTELLA*/
function messaggio(){
	 a=$("#valore2").val();
	 if (a!=0)
	 	alert ("Ricorda che eliminare argomenti significa perdere totalmente il punteggio ottenuto relativamente ad essi!")
     
}

/*FUNZIONI PER MOSTRARE ED ELIMINARE SPIEGAZIONI NELL'OVERLAY DELL'AUTOVALUTAZIONE*/

function dropout2(){
	$("div#spiegazionePunteggio").hide("slow");
}

function punteggio(colore){
	
	var valoreVideo= $("span#value").text();
	
	var valor = parseInt(valoreVideo);
	if(colore == "1"){
		var tot= valor*1;
		$("span#capire").text("non hai ancora capito molto del video. Meglio darsi un po' più di tempo!")
		$("span#colore").text("rosso")
	}
	if(colore == "2"){
		var tot= valor*2;
		$("span#capire").text("ci sei quasi! Ma vuoi ancora avere l'opportunità di migliorarti!")
		$("span#colore").text("giallo")
	}
	if(colore == "3"){
		var tot= valor*3;
		$("span#capire").text("hai capito tutto! sei pronto per il prossimo video!")
		$("span#colore").text("verde")
	}
	
	
	$("span#totale").text(tot)
	$("div#spiegazionePunteggio").show("slow");
}

/* SPIEGAZIONI DELLA HOME PAGE AL PASSAGGI DEL MOUSE */
function fammivedere(valore){
	if(valore== "quattro"){
		$('#quattroN').toggle('show');
		
	}
	if(valore == "uno"){
		$("#unoN").toggle("show");
	}
	if(valore == "due"){
		$("#dueN").toggle("show");
	}
	if(valore == "tre"){
		$("#treN").toggle("show");
	}
	}
	
	

/*MOSTRA L'OVERLAY DEL PARZIALE _VOTO*/
function showOver(){
	$('#myModal').modal('show');
}

/* BARRA DI CARICAMENTO*/
function showContent() {
	document.getElementById("loading").style.display = 'none';
	document.getElementById("content").style.display = 'block';
}

/*CALCOLI RISPETTO A QUANTE CARTELLE L'UTENTE SCEGLIE SIA ALL'ISCRIZIONE SIA AL CAMBIAMENTO*/
var Quanti = 0;
function quanti(elemento) {

	var d = document.getElementById("valore");

	if(elemento.checked) {
		Quanti += 1;
		d.setAttribute("value", Quanti);
	} else {
		Quanti -= 1;
		d.setAttribute("value", Quanti);
	}
	var num = $("#valore").val();
	if(num <= 0) {
		$("#eccomi").attr("disabled", "disabled");
		$("#testoErrore").html("<div class=\"field_with_errors\" >Non puoi selezionare meno di una cartella!</div>");

	} else {
		$("#testoErrore").html("");
		$("#eccomi").removeAttr("disabled");
	}
}

var Quanti1 = 0;
function quanti1(elemento) {

	var d = document.getElementById("valore1");

	if(elemento.checked) {
		Quanti1 += 1;
		d.setAttribute("value", Quanti1);
	} else {
		Quanti1 -= 1;
		d.setAttribute("value", Quanti1);
	}
	var num = $("#valore1").val();
	if(num <= 0) {
		$("#eccomi").attr("disabled", "disabled");
		$("#testoErrore1").html("<div class=\"field_with_errors\" >Non puoi selezionare meno di una cartella!</div>");

	} else {
		$("#testoErrore1").html("");
		$("#eccomi").removeAttr("disabled");
	}
}

var Quanti2 = 0;
function quanti2(elemento) {

	var d = document.getElementById("valore2");

	if(elemento.checked) {
		Quanti2 += 1;
		d.setAttribute("value", Quanti2);
	} else {
		Quanti2 -= 1;
		d.setAttribute("value", Quanti2);
	}
	var num = $("#valore2").val();
	if(num <= 0) {
		$("#eccomi").attr("disabled", "disabled");
		$("#testoErrore2").html("<div class=\"field_with_errors\" >Non puoi selezionare meno di una cartella!</div>");

	} else {
		$("#testoErrore2").html("");
		$("#eccomi").removeAttr("disabled");
	}
}



/* SCELTA DEL COLORE RISPETTO ALL'ARGOMENTO  */
function nascondi(valore, name) {
	var c = $("a[name='" + valore + "']");
	$(c).removeAttr('hidden');
	$("h3#" + valore).remove();
	$("div#" + valore).children().hide("slow");
	if(name == "rosso") {
		$("div#" + valore).append("<img width=\"60\" height=\"60\" src=\"/assets/logo_computer_red.png\" alt=\"Logo_computer_red\">");
	}
	if(name == "giallo") {
		$("div#" + valore).append("<img width=\"60\" height=\"60\" src=\"/assets/logo_computer_giallo.png\" alt=\"Logo_computer_giallo\">");
	}
	if(name == "verde") {
		$("div#" + valore).append("<img width=\"60\" height=\"60\" src=\"/assets/logo_computer_verde.png\" alt=\"Logo_computer_verde\">");
	}
}


/*POP OVER SULLE EMOZIONI*/
$(function ()  { 
	
	$("a#1").popover();
	$("a#2").popover();
	$("a#3").popover();
	$("a#4").popover();  

});  


/* RICARICA DOCUMENTO RICHIAMANDO LA ACTION AGGIORNAMENTO NEL COMMENTS CONTROLLER */
$(document).ready(
         function() {
          setInterval(function() {
            $('.dynamic').load('aggiornamento');
        }, 10000);
    });
    
/* RICARICA DIV DEI COMMENTI*/
$(document).ready(
         function() {
          setInterval(function() {
            $('#comments').load('commentiAggiornamento');
        }, 10000);
    });   

/*FEEDBACK ALL'UTENTE DOPO IL CLICK DELL'EMOZIONE*/
function cambiaEmozione (emozione){
	if(emozione == "1"){
		$("h5#emoz").text("cosi hai detto alla community che sei allegro!").show("slow");
		
	}
	if(emozione == "2"){
		$("h5#emoz").text("cosi hai detto alla community che sei triste!").show("slow");
	}
	if(emozione == "3"){
		$("h5#emoz").text("cosi hai detto alla community che sei Arrabbiato!").show("slow");
	}
	if(emozione == "4"){
		$("h5#emoz").text("cosi hai detto alla community che sei annoiato!").show("slow");
	}
}



$(document).ready(function() {
	/*TOOLTIP PER DESCRIZIONE VIDEO*/
	$("p#tl").tooltip();
	/*SPIEGAZIONI PER I BOTTONI*/
	$("a[name='rosso']").popover();
	$("a[name='verde']").popover();
	$("a[name='giallo']").popover();

	
	$("#eccomi").attr("disabled", "disabled");
	
	/*spiegazioni in home page*/
	$("div.span6").delay(500).show("slow");
	$("div#uno").delay(1000).show("slow");
	$("div#due").delay(2000).show("slow");
	$("div#tre").delay(3000).show("slow");
	$("div#quattro").delay(4000).show("slow");
	$(".emozioni").delay(500).fadeIn("slow");
	
      
      
	

});
var colore = null;

$(document).ready(function() {
	var err = $("div#Presentazione").val();
	if(err != null) {
		$("div#form_application").show();
	}

	var errors = $("div#error_explanation").val();
	if(errors != null) {
		$("div#form_application").show();

	}
});



/*EFFETTI DI SPIEGAZIONE PER LE PAGINE DI REGISTRAZIONE E SIGN IN*/
function dropout(){
	$("div.uscitaInfo").hide("slow");
}

function mostraSuggerimento(x) {
	
	
	if(x== "gravatar"){
		//dropout();
		$('div#signin').html("<h2 id=\"tab\" style=\"display: none\">Modifica la tua immagine del profilo</h2> ");
		$("h2#tab").after("<h3 class=\"testo_spiegazione\">Puoi inserire una tua immagine personale o una qualsiasi immagine che ti rappresenta!</h3>");
		$("h2#tab").show("slide");
		$("div.uscitaInfo").show("slow");
	}

	if(x == "session_email"){
		//dropout();
		$('div#signin').html("<h2 id=\"tab\" style=\"display: none\">Scrivi la tua mail</h2> ");
		$("h2#tab").after("<h3 class=\"testo_spiegazione\">Il modo univoco per identificarti in colorme!</h3>");
		$("h2#tab").show("slide");
		$("div.uscitaInfo").show("slow");
	}
	
	if(x == "session_password"){
		//dropout();
		$('div#signin').html("<h2 id=\"tab\" style=\"display: none\">Scrivi la tua password segreta! </h2> ");
		$("h2#tab").after("<h3 class=\"testo_spiegazione\">Non te la ricordi?</h3>");
		$("h2#tab").show("slide");
		$("div.uscitaInfo").show("slow");
	}
	
	
	

	if(x == "tabella") {
		//dropout();
		$('div#signin').html("<h2 id=\"tab\" style=\"display: none\">Scegli gli argomenti che vuoi imparare!</h2> ");
		$("h2#tab").after("<h3 class=\"testo_spiegazione\">Qui entriamo nel vivo dell'iscrizione a colorme. Puoi scegliere tutte gli argomenti in cui vuoi migliorare. Una volta scelti potrai vedere i video!</h3>");
		$("h2#tab").show("slide");
		$("div.uscitaInfo").show("slow");
	}

	if(x == "user_name") {
		
		$("div#signin").html("<h2 id=\"nome\" style=\"display: none\">Qui inserisci il tuo nome!</h2>");
		$("h2#nome").after("<h3 class=\"testo_spiegazione\">Non deve essere per forza il tuo nome di battesimo! Questo sarà il nome con cui gli altri utenti di colorMe ti conosceranno!</h3>");
		$("h2#nome").fadeIn("slow");
		$("div.uscitaInfo").show("slow");
		

	}
	if(x == "user_email") {
		//dropout();
		$("div#signin").html("<h2 id=\"email\" style=\"display: none\">Qui inserisci la tua mail personale!</h2>");
		$("h2#email").after("<h3 class=\"testo_spiegazione\">La tua mail serve, non solo per aggiornarti con le novità del sito, ma anche per avere un contatto più diretto con i tuoi amici su color me!</h3>");
		$("h2#email").fadeIn("slow");
		$("div.uscitaInfo").show("slow");
		//alert("qui la mail");
	}
	if(x == "user_password") {
		//dropout();
		$("div#signin").html("<h2 id=\"pass\" style=\"display: none\">Qui inserisci la password!</h2>");
		$("h2#pass").after("<h3 class=\"testo_spiegazione\">La tua password è segreta! Ricorda che deve essere superiore alle 6 cifre. Meno caratteri sarebbero una password troppo insicura!</h3>");
		$("h2#pass").show("slide");
		
		$("div.uscitaInfo").show("slow");
	}
	if(x == "user_password_confirmation") {
		
		//dropout();
		$("div#signin").html("<h2 id=\"passC\" style=\"display: none\">Riscrivi la tua password segreta!</h2>");
		$("h2#passC").after("<h3 class=\"testo_spiegazione\">Non ti preoccupare! La tua password è corretta! Dobbiamo solo controllare che sia veramente quella che vuoi.</h3>");
		$("h2#passC").show("slide");
		
		$("div.uscitaInfo").show("slow");
	}
}

