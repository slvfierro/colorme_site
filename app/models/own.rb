class Own < ActiveRecord::Base
  
  attr_accessible :scorefolder, :state, :user_id, :folder_id
  
  #COLLEGAMENTO TRA LA RELAZIONE OWN E LA RELAZIONE UTENTE NELL'ASSOCIAZIONE M-N TRA UTENTE E CARTELLA
  belongs_to :user, class_name:"User"
  belongs_to :folder, class_name:"Folder"
 
  #VALIDAZIONI
  validates :user_id, presence: true
  validates :folder_id, presence: true 
  
  default_scope order: 'owns.created_at DESC' 
  
end
