class Video < ActiveRecord::Base
  attr_accessible :title, :valuevideo, :url, :folder_id, :description
  
  #GESTIONE DELLA RELAZIONE M-N (WATCHES) TRA VIDEO E UTENTE
  has_many :watches, foreign_key: "video_id", dependent: :destroy
  has_many :users, :through => :watches, :source => :user 
  
  #GESTIONE DELLA RELAZIONE M-N (COMMENTS) TRA COMMENTI E VIDEO
  has_many :comments, foreign_key: "video_id", dependent: :destroy
  has_many :users, :through => :comments, :source => :user  
  
  #GESTIONE RELAZIONE 1-N (CREA ) TRA UTENTE E VIDEO
  belongs_to :user
  
  #RELAZIONE TRA I VIDEO E LE CARTELLE
  belongs_to :folder 
  
  validates :user_id, presence: true
  validates :title,:presence => true,:length => {:maximum=>50}
  validates :url, :presence=>true,:uniqueness=>{:case_sensitive=>false, :message=>"case"}
  validates :valuevideo, :presence=>true
  
end