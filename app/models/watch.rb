class Watch < ActiveRecord::Base
  
  attr_accessible :user_id, :video_id, :votecolor
  
  #GESTIONE DELLA RELAZIONE M-N (VEDE) TRA UTENTE E VIDEO
  belongs_to :user, class_name:"User"
  belongs_to :video, class_name:"Video"
  
end
