class Folder < ActiveRecord::Base
  attr_accessible :namefolder
  
  #RELAZIONE CREA, TRA UTENTE E CARTELLA
  belongs_to :user
  
  #RELAZIONE POSSIDE, TRA CARTELLA E VIDEO
  has_many :videos
  
  #GENERO RELAZIONE M-M TRA UTENTE E CARTELLE
  has_many :owns, foreign_key: "folder_id", dependent: :destroy
  has_many :users, :through => :owns, :source => :user  
    
  #DOWNCASE DEL NOME DELLA CARTELLA, PER VALIDAZIONE SUCCESSIVA
  before_save {|folder| folder.namefolder = namefolder.downcase}
  
  
  #CONTROLLI DI VALIDAZIONE
  validates :user_id, presence: true
  validates :namefolder,:presence => true,:length => {:maximum=>20}, :uniqueness=>{:case_sensitive=>false, :message=>"esistente"}
  
  #ORDINAMENTO DELLE TUPLE IN ORDINE DECRESCENTE
  default_scope order: 'folders.created_at DESC' 
  
end
