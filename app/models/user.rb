class User < ActiveRecord::Base
  attr_accessible :name,:email, :password, :password_confirmation
    
  #GESTIONE DELLA RELAZIONE M-N (OWN) TRA UTENTE E CARTELLA
  has_many :owns, foreign_key: "user_id", dependent: :destroy
  has_many :folders, :through => :owns, :source => :folder
  
  #GESTIONE DELLA RELAZIONE 1-N (CREA) TRA UTENTE E CARTELLA
  has_many :folders, dependent: :destroy 
  
  #GESTIONE DELLA RELAZIONE M-N (COMMENTS) TRA UTENTE E VIDEO
  has_many :comments, foreign_key: "user_id", dependent: :destroy
  has_many :videos, :through => :comments, :source => :video  
  
  #GESTIONE DELLA RELAZIONE M-N (WATCHES) TRA UTENTE E VIDEO
  has_many :watches, foreign_key: "user_id", dependent: :destroy
  has_many :videos, :through => :watches, :source=> :video
  
  #GESTIONE DELLA RELAZIONE 1-N (CREA) TRA UTENTE E VIDEO 
  has_many :videos, dependent: :destroy 
  
  #CRIPTO LA PASS DELL'UTENTE
  has_secure_password 
  #REGULAR EXPRESSION PER IL CONTROLLO MAIL
  email_regex=/\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i 
  
  
  before_save {|user| user.email = email.downcase}
  #UTILE PER LA CREAZIONE DELLA SESSIONE UTENTE
  before_save :create_remember_token
   
  
  #VALIDAZIONI
  validates :email, :presence=>true,:format=>{:with => email_regex, :message=>"invalida"},:uniqueness=>{:case_sensitive=>false, :message=>"esistente"}
  validates :name, :presence=>true,:uniqueness=>{:case_sensitive=>false, :message=>"Nome esistente. Inserisci un altro nome"}
  validates :password, :length=>{:within=>6..40, :message=>" - Bisogna inserire una password di lunghezza tra 4 e 6 caratteri"} 
  validates_presence_of :password,  :message=>"Non puo esserci una password vuota!"
  validates_presence_of :name,:message=>"Il nome dev'essere pieno"
  validates_presence_of :email,:message=>"E' obbligatoria la mail"
  validates_confirmation_of :password, :message=>"Riscrivi la password per confermarla"
  
  
  def self.authenticate(email,submitted_password)
    user=find_by_email(email)
  end
  
  private 
  
  def create_remember_token
    self.remember_token=SecureRandom.urlsafe_base64
  end

  def secure_hash(string)
    Digest::SHA2.hexdigest(string)
  end  
  
end

