class Comment < ActiveRecord::Base
  attr_accessible :text, :user_id, :video_id
  
  # GENERANO RELAZIONE M-N TRA UTENTE E VIDEO
  belongs_to :user, class_name:"User"
  belongs_to :video, class_name:"Video"  
  
  #I COMMENTI VENGONO VISUALIZZATI IN ORDINE DESCRESCENTE PER L'ATTRIBUTO CREATED_AT
  default_scope order: 'comments.created_at DESC'
  
end
