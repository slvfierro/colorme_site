class FoldersController < ApplicationController
before_filter :signed_in_user
before_filter :admin_user, only: [:new, :create, :destroy]

 
 def new
   @folder=Folder.new
 end
  
 def create
    @folder = current_user.folders.new(params[:folder])
    if @folder.save
      flash[:success] = "La cartella e' stata creata!"
      redirect_to root_path
  else
    flash[:error]="Errore!"
    render 'new'
  end
 end
 
 def edit
   
 end
 
 def update
   
 end
 
 
end