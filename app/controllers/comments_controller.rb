class CommentsController < ApplicationController
   before_filter :signed_in_user #l'utente può accedere alle action edit e update solo se loogato!
 
  def index    
    @@a = Object.new
    @url = params[:url]
    @comment=Comment.new
    @commento=Comment.new
    @comments = Comment.find(:all,:order => 'created_at DESC', :conditions=>['video_id == ?',params[:video_id].to_i])
    @user = User.new
    
    @video = params[:video_id]
    @@c = @video
    
    
    @folder = params[:folder_id]
    @@a = @folder
    @b = @@a
    
    @own = Own.new
    @userownfolder=Own.find(:first,:order => 'created_at DESC', :conditions=>['user_id==? AND folder_id == ?',current_user.id,params[:folder_id].to_i])
  
  end  
    
  def create  
    @comment = Comment.create(:user_id=>current_user.id,:video_id=>params[:video_id].to_i,:text => params[:text])     
    respond_to do |format|  
      if @comment.save        
        @comments = Comment.find(:all,:order => 'created_at DESC', :conditions=>['video_id == ?',params[:video_id].to_i])  
        format.html { redirect_to comments_path }
        format.js
      else  
        flash[:notice] = "Messaggio non creato"  
        format.html { redirect_to comments_path }  
      end  
    end  
  end
  
  def commentiAggiornamento
    @d = @@c
    puts @d
    @comments = Comment.find(:all,:order => 'created_at DESC', :conditions=>['video_id == ?', @d.to_i])
    render :partial => @comments 
  end
  
  def aggiornamento
    @b = @@a
    render :partial => "layouts/utenticartella"
  end
         
  def emotion
      @user = current_user.update_attribute(:emotion, params[:emozione].to_i)
      respond_to do |format|
        format.html {redirect_to :back}
      end
      sign_in current_user
  end
  end
