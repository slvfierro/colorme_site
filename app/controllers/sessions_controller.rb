class SessionsController < ApplicationController
  def new
    if signed_in?
      redirect_to root_path
    end
  end

  def create
    user=User.find_by_email(params[:session][:email])
    if user && user.authenticate(params[:session][:password])
      sign_in user
      redirect_back_or root_path
      flash[:error]="benvenuto"
    #fai loggare l'utente
    else
      flash[:error]='La combinazione mail e password sbagliata!'
      #render 'new'
      redirect_to :back
    end
  end

  def destroy
    sign_out
    redirect_to root_path
  end

 
 
end
