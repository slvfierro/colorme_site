class VideosController < ApplicationController
before_filter :signed_in_user
before_filter :admin_user, only: [:show, :index, :edit, :update, :destroy, :new]

  def new   
    array=[]
    @video=Video.new
    @comment=Comment.new
    Folder.find_each do |folder|    
      array << folder.namefolder
    end
    @nameoffolders=array
  end
  
 
  def create
    b=0
    Folder.find_each do |folder|    
       if folder.namefolder==params[:folder_name]
         b=folder.id
       end
    end
    @video = current_user.videos.new(params[:video])
    @video.folder_id=b    
    if @video.save
      @comment=Comment.create(:user_id=>current_user.id,:video_id=>@video.id, :text=>"Ciao commenta il video che ho inserito!")
      flash[:success] = "Il video e' stato inserito!"
      redirect_to root_path
    else
      flash[:error]="Errore! Video non valido o video gia' presente"
      redirect_to :back
    end
  end
  
  def show
     
  end 
  
  
end