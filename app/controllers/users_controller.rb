class UsersController < ApplicationController
   
  before_filter :signed_in_user, only: [:index, :edit, :update] 
  #l'utente può accedere alle action edit e update solo se loogato!
  before_filter :correct_user,   only: [:edit, :update]
 
 
  def new
    if signed_in?
      redirect_to root_path
    end
    @user=User.new
    @own=Own.new
  end
  
  def index
       
  end
  
   
  def edit
    @user = User.find(params[:id])
  end  

    
  def update
    @user = User.find(params[:id])
    if (@user.update_attribute(:name,params[:user][:name]) &&  @user.update_attribute(:email,params[:user][:email]))      
      flash[:success]="Profilo Aggiornato!"
      sign_in @user
      redirect_to root_path
    else
      flash[:error]="Ci sono dei campi errati"
      render 'edit'
    end    
  end
  
  def viewusers    
    respond_to do |format|  
        format.html { redirect_to :back }
        format.js      
    end     
  end
  
  def show
    @user=User.find(params[:id]) #User class is in model folder! 
  end
  
  def create
    if params[:valore]=="0"
      flash[:error]="Devi almeno scegliere una cartella!"
      redirect_to :back
    else
      totfolders=params[:rfolder].count      
      @user=User.new(params[:user])          
      if @user.save
        sign_in @user
        for i in (0..totfolders-1)
          @own=Own.create(:user_id=>current_user.id,:folder_id=>params[:rfolder][i],:scorefolder=>0,:state=>0)
        end        
        flash[:success] = "Benvenuto in colorme! Inzia a dare un colore alle tue conoscenze informatiche!"
        redirect_to root_path
       else
        flash[:error] = "Ci sono errori!!!"
        redirect_to :back
      end 
     end   
  end 
  
   private
          
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_path) unless current_user?(@user) #unless=a meno che l'utente non sia il current user
    end
    
    

end
